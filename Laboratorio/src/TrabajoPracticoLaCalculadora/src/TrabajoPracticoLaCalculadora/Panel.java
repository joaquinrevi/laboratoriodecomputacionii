package TrabajoPracticoLaCalculadora;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

class PanelC extends JPanel {
    private JButton pantalla;
    private JPanel numeros;
    private double resultado;
    private String ultimaoperacion;
    boolean comienzo;
    public PanelC(){
        setLayout(new BorderLayout());
        pantalla = new JButton("0");
        comienzo = true;
        pantalla.setEnabled(false);
        add(pantalla, BorderLayout.NORTH);

        numeros = new JPanel();
        numeros.setLayout(new GridLayout(4,4));

        AccionNumero nroAccion = new AccionNumero();
        AccionOp Op = new AccionOp();

        nuevoNro("7", nroAccion);
        nuevoNro("8", nroAccion);
        nuevoNro("9", nroAccion);
        nuevaOp("X", Op);

        nuevoNro("4", nroAccion);
        nuevoNro("5", nroAccion);
        nuevoNro("6", nroAccion);
        nuevaOp("-", Op);

        nuevoNro("1", nroAccion);
        nuevoNro("2", nroAccion);
        nuevoNro("3", nroAccion);
        nuevaOp("+", Op);

        nuevaOp("/", Op);
        nuevoNro("0", nroAccion);
        JButton BotonVacio = new JButton();
        numeros.add(BotonVacio);
        nuevaOp("=", Op);

        add(numeros,BorderLayout.CENTER);
        ultimaoperacion  = "=";
    }
    private void nuevoNro(String text, AccionNumero oyente){
        JButton boton = new JButton(text);
        boton.addActionListener(oyente);
        numeros.add(boton);
    }

    private void nuevaOp(String text, AccionOp oyente){
        JButton boton = new JButton(text);
        boton.addActionListener(oyente);
        numeros.add(boton);
    }
    private class AccionNumero implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String numero =  e.getActionCommand();
            if(comienzo){
                pantalla.setText(numero);
                comienzo = false;
            }else {
                pantalla.setText(pantalla.getText()+numero);
            }
        }
    }
    private class AccionOp implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String operacion = e.getActionCommand();
            calcular(Double.parseDouble(pantalla.getText()));
            ultimaoperacion = operacion;
            comienzo = true;

        }
        public void calcular(double num){
            if(ultimaoperacion.equals("+")){
                resultado += num;
            }
            if(ultimaoperacion.equals("-")){
                resultado -= num;
            }
            if(ultimaoperacion.equals("/")){
                resultado /= num;
            }
            if(ultimaoperacion.equals("X")){
                resultado *= num;
            }
            if(ultimaoperacion.equals("=")){
                resultado = num;
            }
            pantalla.setText(""+resultado);

        }
    }
    class mause implements MouseMotionListener{

        @Override
        public void mouseDragged(MouseEvent e) {
                
        }

        @Override
        public void mouseMoved(MouseEvent e) {

        }
    }

}
