package TrabajoIndividualBanco;

import java.util.Random;

public class CuentaCorriente{
    private String titular;
    private double saldo;
    private int n_cuenta;

    public CuentaCorriente(String titular, double saldo) {
        this.titular = titular;
        this.saldo = saldo;
        Random x = new Random();
        this.n_cuenta = Math.abs(x.nextInt());
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public int getN_cuenta() {
        return n_cuenta;
    }

    public void IngresarDinero (double dinero){
        if(dinero < 0){
            System.out.println("ERROR no puede ingresar numeros negativos");
        }
        else {
            setSaldo(getSaldo() + dinero);
        }
    }

    public void SacarDinero (double dinero){
        if((getSaldo() - dinero) < 0){
            System.out.println("ERROR el saldo no puede quedar en negativo");
        }
        else {
            setSaldo(getSaldo() - dinero);
        }
    }
    public String MostrarSaldo (){
        return "El Saldo de la Cuenta Nº " + n_cuenta +", Del usuario "+ titular +" Posee un saldo actual de $" + saldo;
    }

    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "titular='" + titular + '\'' +
                ", saldo=" + saldo +
                ", n_cuenta=" + n_cuenta +
                "}\n";
    }


    public static void Transferencia(CuentaCorriente emisor, CuentaCorriente receptor, double dinero) {
        if((emisor.getSaldo() - dinero) < 0 || dinero < 0){
            System.out.println("ERROR no puede ingresar un valor negativo o el emisor no puede quedar con saldo negativo");
        }
        else {
            receptor.setSaldo(receptor.getSaldo() + dinero);
            emisor.setSaldo(emisor.getSaldo() - dinero);
        }
        }
    }


