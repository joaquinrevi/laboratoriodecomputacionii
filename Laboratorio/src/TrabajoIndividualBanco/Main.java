package TrabajoIndividualBanco;

import java.io.FileWriter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        CuentaCorriente c1 = new CuentaCorriente("Martin Perez", 3500);
        CuentaCorriente c2 = new CuentaCorriente("Jesica Martinez",10000);
        CuentaCorriente c3 = new CuentaCorriente("Marcos Perez", 20000);
        CuentaCorriente c4 = new CuentaCorriente("Romina Vallejos", 6000);
        int eleccion;
        double dinero;
        int numcuenta;
        boolean x;
        Set<CuentaCorriente> Cuentas = new HashSet<CuentaCorriente>();
        Cuentas.add(c1);
        Cuentas.add(c2);
        Cuentas.add(c3);
        Cuentas.add(c4);
        try {
            do {
                Scanner sc = new Scanner(System.in);
                System.out.println("\n¿Que acción desea realizar?" +
                        "\n1 = Ingresar Dinero a una cuenta" +
                        "\n2 = Sacar Dinero de una cuenta" +
                        "\n3 = Mostrar el Saldo de una cuenta" +
                        "\n4 = Mostrar Datos de una cuenta" +
                        "\n5 = Hacer  una Transferencia" +
                        "\n6 = Listar las Cuentas cargadas" +
                        "\n7 = Almacenar en un archivo las cuentas cargadas" +
                        "\n\nIngrese 0 (cero) para Salir");
                eleccion = sc.nextInt();
                System.out.println("\n");
                switch (eleccion) {
                    case 1:
                        System.out.println("Ingrese el número de cuenta del usuario");
                        numcuenta = sc.nextInt();
                        x = false;
                        for (CuentaCorriente cliente : Cuentas) {
                            if (cliente.getN_cuenta() == numcuenta) {
                                x = true;
                                System.out.println("\nBienvenido " + cliente.getTitular());
                                System.out.print("¿Cuánto Dinero desea ingresar? \n$");
                                dinero = sc.nextDouble();
                                cliente.IngresarDinero(dinero);

                            }
                        }
                        if (x == false) {
                            for (int a = 0; a < 3; a++) {
                                System.out.println("El usuario no fue encontrado");
                            }
                        }
                        break;
                    case 2:
                        System.out.println("Ingrese el número de cuenta del usuario");
                        numcuenta = sc.nextInt();
                        x = false;
                        for (CuentaCorriente cliente : Cuentas) {
                            if (cliente.getN_cuenta() == numcuenta) {
                                x = true;
                                System.out.println("\nBienvenido " + cliente.getTitular());
                                System.out.print("¿Cuánto Dinero desea extraer? \n$");
                                dinero = sc.nextDouble();
                                cliente.SacarDinero(dinero);

                            }
                        }
                        if (x == false) {
                            for (int a = 0; a < 3; a++) {
                                System.out.println("El usuario no fue encontrado");
                            }
                        }
                        break;
                    case 3:
                        System.out.println("Ingrese el número de cuenta del usuario");
                        numcuenta = sc.nextInt();
                        x = false;
                        for (CuentaCorriente cliente : Cuentas) {
                            if (cliente.getN_cuenta() == numcuenta) {
                                x = true;
                                System.out.println("\n" + cliente.MostrarSaldo());
                            }
                        }
                        if (x == false) {
                            for (int a = 0; a < 3; a++) {
                                System.out.println("El usuario no fue encontrado");
                            }
                        }
                        break;
                    case 4:
                        System.out.println("Ingrese el número de cuenta del usuario");
                        numcuenta = sc.nextInt();
                        x = false;
                        for (CuentaCorriente cliente : Cuentas) {
                            if (cliente.getN_cuenta() == numcuenta) {
                                x = true;
                                System.out.println("\n" + cliente.toString());
                            }
                        }
                        if (x == false) {
                            for (int a = 0; a < 3; a++) {
                                System.out.println("El usuario no fue encontrado");
                            }
                        }
                        break;
                    case 5:
                        System.out.println("Ingrese su número de usuario");
                        numcuenta = sc.nextInt();
                        int numcuentareceptor;
                        x = false;
                        boolean y = false;
                        for (CuentaCorriente clienteemisor : Cuentas) {
                            if (clienteemisor.getN_cuenta() == numcuenta) {
                                x = true;
                                System.out.println("\nBienvenido " + clienteemisor.getTitular());
                                System.out.println("Ingrese el número de cuenta al que desea transferir dinero");
                                numcuentareceptor = sc.nextInt();
                                for (CuentaCorriente clientereceptor : Cuentas) {
                                    if (clientereceptor.getN_cuenta() == numcuentareceptor && clienteemisor.getN_cuenta() != clientereceptor.getN_cuenta()) {
                                        y = true;
                                        System.out.print("\n¿Cuándo dinero desea transferir al usuario "+clientereceptor.getTitular()+"? \n$");
                                        dinero = sc.nextDouble();
                                        CuentaCorriente.Transferencia(clienteemisor, clientereceptor, dinero);
                                    }
                                }
                                if (y == false) {
                                    for (int a = 0; a < 3; a++) {
                                        System.out.println("El usuario a transferir no fue encontrado o el número de cuenta del emisor es igual al del receptor");
                                    }
                                }
                            }
                        }
                        if (x == false) {
                            for (int a = 0; a < 3; a++) {
                                System.out.println("Su usuario no fue encontrado");
                            }
                        }
                        break;
                    case 6:
                        for (CuentaCorriente cliente : Cuentas) {
                            System.out.println(cliente.toString());
                        }
                        break;
                    case 7:
                        System.out.println("Almacenando Datos...");
                        try {
                            FileWriter escritura = new FileWriter("C:/Users/Administrador.Reviriego-PC/Desktop/UTN/Cuentas.txt");
                            for (CuentaCorriente cliente : Cuentas) {
                                for (int i = 0; i < cliente.toString().length(); i++) {
                                    escritura.write(cliente.toString().charAt(i));
                                }
                            }
                            System.out.println("\nLos Datos han sino almacenados exitosamente");
                            escritura.close();
                        } catch (Exception e) {
                            System.out.println("\nERROR el archivo no fue encontrado o no Existe");;
                        }
                        break;
                    case 0:
                        System.out.println("¡Hasta Luego!");
                        break;
                    default:
                        System.out.println("Opcion Invalida, Ingrese de nuevo");
                }

            } while (eleccion != 0);
        } catch (Exception e) {
            System.out.println("ERROR el dato ingresado no es un número entero");
        }
    }

}