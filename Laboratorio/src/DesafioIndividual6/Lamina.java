package DesafioIndividual6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class Lamina extends JPanel {

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        setLayout(null);
        JTextField Cuadro1 = new JTextField();
        JTextField Cuadro2 = new JTextField();

        Cuadro1.setBounds(120, 10, 200, 30);
        Cuadro2.setBounds(120, 70, 200, 30);

        add(Cuadro1);
        add(Cuadro2);

        EventoFoco evento = new EventoFoco();

        Cuadro1.addFocusListener(evento);

    }
         private class EventoFoco implements  FocusListener{

            @Override
            public void focusGained(FocusEvent e) {
                System.out.println("El Cuadro 1 Ha Ganado el Foco");;

            }

            @Override
            public void focusLost(FocusEvent e) {
                System.out.println("El Cuadro 1 Ha Perdido el Foco");
            }
        }
    }

