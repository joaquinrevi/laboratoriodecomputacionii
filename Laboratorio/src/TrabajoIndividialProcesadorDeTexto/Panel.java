package EditorDeTexto;

import javax.swing.*;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.*;

public class Panel extends JPanel {

    JTextPane areatexto;
    JMenu tamanio;
    JMenu fuente;
    JMenu estilo;
    JMenu archivo;
    public static boolean guardado = true;

    public Panel() {
        setLayout(new BorderLayout());
        JPanel menu = new JPanel();
        JMenuBar barra = new JMenuBar();

        archivo = new JMenu("Archivo");
        tamanio = new JMenu("Tamaño");
        fuente = new JMenu("Fuente");
        estilo = new JMenu("Estilo");

        JMenuItem abrir = new JMenuItem("Abrir Archivo");
        JMenuItem guardar = new JMenuItem("Guardar Archivo");


        archivo.add(abrir);
        archivo.add(guardar);

        abrir.addActionListener(new abrirListener());
        guardar.addActionListener(new guardarListener());

        ButtonGroup tamanio_letra = new ButtonGroup();
        JRadioButtonMenuItem ocho = new JRadioButtonMenuItem("8");
        JRadioButtonMenuItem doce = new JRadioButtonMenuItem("12");
        JRadioButtonMenuItem dieciseis = new JRadioButtonMenuItem("16");
        JRadioButtonMenuItem veinte = new JRadioButtonMenuItem("20");
        JRadioButtonMenuItem veinticuatro = new JRadioButtonMenuItem("24");
        tamanio_letra.add(doce);
        tamanio_letra.add(dieciseis);
        tamanio_letra.add(veinte);
        tamanio_letra.add(veinticuatro);

        ocho.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 8));
        doce.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 12));
        dieciseis.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 16));
        veinte.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 20));
        veinticuatro.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamano", 24));

        tamanio.add(ocho);
        tamanio.add(doce);
        tamanio.add(dieciseis);
        tamanio.add(veinte);
        tamanio.add(veinticuatro);

        eleccionmenu("Arial", "fuente", "Arial", 9, 10);
        eleccionmenu("Times New Roman", "fuente", "Times New Roman", 9, 12);
        eleccionmenu("Verdana", "fuente", "Verdana", 9, 12);
        eleccionmenu("Negrita", "estilo","", Font.BOLD, 1);
        eleccionmenu("Cursiva", "estilo","", Font.ITALIC, 1);

        barra.add(tamanio);
        barra.add(fuente);
        barra.add(estilo);
        barra.add(archivo);

        menu.add(barra);
        add(menu, BorderLayout.NORTH);
        areatexto = new JTextPane();
        add(areatexto, BorderLayout.CENTER);
        JScrollPane scrollbar = new JScrollPane(areatexto);
        add(scrollbar,BorderLayout.CENTER);
        JPopupMenu menuopciones = new JPopupMenu();
        JMenuItem negrita = new JMenuItem("Negrita");
        JMenuItem cursiva = new JMenuItem("Cursiva");
        negrita.addActionListener(new StyledEditorKit.BoldAction());
        cursiva.addActionListener(new StyledEditorKit.ItalicAction());
        menuopciones.add(negrita);
        menuopciones.add(cursiva);
        areatexto.setComponentPopupMenu(menuopciones);

    }

    public void eleccionmenu(String texto, String menu, String tipoletra, int tipoestilo, int tamanioletra){
        JMenuItem elemmenu = new JMenuItem(texto);

        if(menu.equals("fuente")) {
            fuente.add(elemmenu);
            if (tipoletra.equals("Arial")) {
                elemmenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra", "Arial"));
            } else if(tipoletra.equals("Times New Roman")) {
                elemmenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra", "Times New Roman"));
            } else if(tipoletra.equals("Verdana")){
                elemmenu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra", "Verdana"));
            }
        }
        else if(menu.equals("estilo")){
            estilo.add(elemmenu);
            if(tipoestilo == Font.BOLD){
                elemmenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
                elemmenu.addActionListener(new StyledEditorKit.BoldAction());
            }
            else if(tipoestilo == Font.ITALIC){
                elemmenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
                elemmenu.addActionListener(new StyledEditorKit.ItalicAction());
            }
        }
        else if(menu.equals("tamanio")){
            tamanio.add(elemmenu);
            elemmenu.addActionListener(new StyledEditorKit.FontSizeAction("cambia_letra", tamanioletra));
        }


    }
    private class guardarListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{

                JFileChooser elegirArchivo = new JFileChooser();
                elegirArchivo.showSaveDialog(areatexto);
                File guardararchivo = (File) elegirArchivo.getCurrentDirectory();
                String ruta = guardararchivo+"\\"+elegirArchivo.getSelectedFile().getName();
                ObjectOutputStream escribiendoFichero = new ObjectOutputStream(new FileOutputStream(ruta) );
                escribiendoFichero.writeObject(areatexto.getText());
                escribiendoFichero.close();
                JOptionPane.showConfirmDialog(null, "Archivos Guardados", "Mensaje", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
                guardado = false;



            } catch (Exception exception) {
                JOptionPane.showConfirmDialog(null, "No fue posible guardar los datos", "ERROR", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
            }

        }
    }
    private class abrirListener implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                JFileChooser elegirArchivo = new JFileChooser();
                elegirArchivo.showOpenDialog(areatexto);
                String cargararchivo = elegirArchivo.getSelectedFile().getAbsolutePath();
                ObjectInputStream fichero = new ObjectInputStream(new FileInputStream(cargararchivo));
                areatexto.setText((String) fichero.readObject());
                fichero.close();


            } catch (Exception exception) {
                JOptionPane.showConfirmDialog(null, "Archivo no Encontrado", "ERROR", JOptionPane.CLOSED_OPTION, JOptionPane.PLAIN_MESSAGE);
            }

        }
    }

}
