package com.company;

import java.util.Random;


public class UsoCuenta{

    public static void main(String[] args) {
        CuentaCorriente cta1 = new CuentaCorriente("Pepito", 4000);
        CuentaCorriente cta2 = new CuentaCorriente("Perlita", 2000);

        CuentaCorriente.Transferencia(cta1, cta2, 2500);

        System.out.println(cta1.toString());
        System.out.println(cta2.toString());


    }
}

    class CuentaCorriente{
        private double saldo;
        private String nombreTitular;
        private long NumeroCuenta;

        public CuentaCorriente(String nombreTitular, double saldo) {
            this.nombreTitular = nombreTitular;
            this.saldo = saldo;
            Random x = new Random();
            this.NumeroCuenta = Math.abs(x.nextLong());

        }

        public CuentaCorriente (String nombreTitular) {
            this.nombreTitular = nombreTitular;
            this.saldo = 0;
            Random x = new Random();
            this.NumeroCuenta = Math.abs(x.nextLong());

        }
        public String ingresarDiner(double saldo){
            if(saldo > 0){
                this.saldo += saldo;
                return "Se ha ingresado $"+ saldo;
            }
            else{
                return "No se puede ingresar un valor negativo";
            }
        }

        public static void Transferencia(CuentaCorriente ctaEntrada, CuentaCorriente ctaSalida, double saldo) {
            ctaEntrada.saldo += saldo;
            ctaSalida.saldo -= saldo;
        }
        @Override
        public String toString() {
            return "\n nombreTitular = " + nombreTitular +
                    "\n saldo = " + saldo +
                    "\n numeroCuenta = " + NumeroCuenta + "\n";
        }

    }

