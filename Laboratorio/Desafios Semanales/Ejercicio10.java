package com.Laboratorio;
import java.util.Scanner;
public class Ejercicio10 {
    public static void main(String[] args) {
        int num;
        int cont = 0;
        Scanner sc = new Scanner(System.in);
        double random = Math.floor(Math.random()*(1-100)+100);
        do{
            System.out.println("Ingrese un numero entre el 1 y el 100: ");
            num = sc.nextInt();
            if(num < 1 || num > 100){
                System.out.println("El numero ingresado esta fuera de los parametros, por favor ingrese de nuevo");
                continue;
            }
            if(num > random){
                System.out.println("El numero ingresado es mayor al esperado ");
                cont++;
            }
            if(num < random){
                System.out.println("El numero ingresado es menor al esperado ");
                cont++;
            }
            if(num == random){
                System.out.println("Correcto! ");
            }


        }while(num != random);
        System.out.println("El numero de intentos hasta descubrir el numero fueron: "+ cont);
    }
}
